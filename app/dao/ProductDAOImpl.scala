package dao

import javax.inject.Inject
import play.api.libs.json.Json
import play.modules.reactivemongo.ReactiveMongoApi
import models.core.product.Product
import service.ProductService
import reactivemongo.play.json._
import reactivemongo.play.json.collection._

import scala.concurrent.{ExecutionContext, Future}

class ProductDAOImpl @Inject()(val reactiveMongoApi: ReactiveMongoApi)(implicit ex: ExecutionContext) extends ProductService {

  def products = reactiveMongoApi.database.map(_.collection[JSONCollection]("products"))

  override def save(product: Product): Future[Product] = products.flatMap(_.insert(product)).flatMap(_ => Future.successful(product))

  override def get(productSlug: String): Future[Option[Product]] = products.flatMap(_.find(Json.obj("slug" -> productSlug)).one[Product])

  override def update(product: Product): Future[Boolean] = {
    val query = Json.obj("slug" -> product.slug)
    val modifier = Json.obj("$set" -> Json.toJson(product))
    products.flatMap(_.update(query, modifier)).flatMap(_ => Future.successful(true))
  }

  override def delete(productSlug: String): Future[Boolean] = {
    val query = Json.obj("slug" -> productSlug)
    products.flatMap(_.remove(query)).flatMap(_ => Future.successful(true))
  }

  override def getAll(): Future[List[Product]] = {
    val query = Json.obj()
    products.flatMap(_.find(query).cursor[Product]().collect[List]())
  }
}
